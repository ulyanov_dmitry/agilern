import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1
  },
  headingWrapper: {
    flex: 3,
    justifyContent: 'flex-end'
  },
  heading: {
    alignSelf: 'center',
  },
  form: {
    flex: 3,
    justifyContent: 'center',
  },
  errorWrapper: {
    flex: 2,
    justifyContent: 'flex-start',
  },
  error: {
    color: 'red',
    alignSelf: 'center',
  },
  login: {
    flex: 4,
    justifyContent: 'flex-start',
  },
});