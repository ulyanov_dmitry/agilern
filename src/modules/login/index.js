import * as React from 'react';
import { connect } from 'react-redux';
import { View, Dimensions } from 'react-native';
import { withOrientation } from '../../helper/orientation';

import styles from './styles';

import * as profileActions from '../../redux/profile';

import Heading from '../../common/heading';
import Text from '../../common/text';
import Input from '../../common/input';
import Button from '../../common/button';

const mapStateToProps = state => {
  const { profile: { signInError } } = state;
  return { signInError };
};

const mapDispatchToProps = dispatch => ({
  signIn: credentials => dispatch(profileActions.signIn(credentials)),
});

class LoginScreen extends React.Component {
  state = {
    username: '',
    password: '',
  };
  
  render() {
    const {
      username,
      password,
    } = this.state;
    const {
      signInError,
    } = this.props;
    
    return (
      <View style={styles.container}>
        <View style={styles.headingWrapper}>
          <Heading style={styles.heading}>Welcome</Heading>
        </View>
        <View style={styles.form}>
          <Input icon='torso'
                 placeholder='username'
                 onChangeText={this.onChange('username')}
                 value={username} />
          <Input icon='lock'
                 placeholder='password'
                 secureTextEntry={true}
                 onChangeText={this.onChange('password')}
                 value={password} />
        </View>
        <View style={styles.errorWrapper}>
          { signInError && <Text style={styles.error}>{signInError}</Text> }
        </View>
        <View style={styles.login}>
          <Button text='Log In'
                  onPress={this.onSubmit} />
        </View>
      </View>
    );
  }
  
  onSubmit = () => {
    const { username, password } = this.state;
    this.props.signIn({ username, password });
  };
  
  onChange = field => value => this.setState(state => ({
    ...state,
    [field]: value,
  }));
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);