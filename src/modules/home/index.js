import * as React from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import styles from './styles';

import Heading from '../../common/heading';

import * as profileSelectors from '../../redux/profile/selectors';

const mapStateToProps = state => ({
  username: profileSelectors.getUsername(state),
});

class Home extends React.Component {
  render() {
    const { username } = this.props;
    
    return (
      <View style={styles.container}>
        <Heading>Hello, {username}!</Heading>
      </View>
    );
  }
}

export default connect(mapStateToProps)(Home);