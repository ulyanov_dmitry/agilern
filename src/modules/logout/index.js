import * as React from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import styles from './styles';

import Heading from '../../common/heading';
import Button from '../../common/button';

import * as profileActions from '../../redux/profile';

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(profileActions.signOut())
});

class Logout extends React.Component {
  render() {
    const { logout } = this.props;
    
    return (
      <View style={styles.container}>
        <Heading style={styles.heading}>Good Bye!</Heading>
        <Button text="Log Out" onPress={logout}/>
      </View>
    );
  }
}

export default connect(null, mapDispatchToProps)(Logout);