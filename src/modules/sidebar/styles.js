import { StyleSheet, Dimensions } from 'react-native';
import * as Styles from '../../styles';

const { dWidth, dHeight } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    paddingTop: 20,
    width: dWidth * 0.8,
    height: dHeight,
    flex: 1,
    backgroundColor: Styles.colors.primary,
  },
  menu: {
    padding: 10,
    marginBottom: 6,
    backgroundColor: Styles.colors.primaryLighterWithOpacity,
  },
  menuText: {
    color: Styles.colors.light,
  }
});