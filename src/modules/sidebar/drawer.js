import * as React from 'react';
import RNDrawer from 'react-native-drawer';
import { DefaultRenderer } from 'react-native-router-flux';
import dismissKeyboard from '../../helper/dismiss-keyboard';

const drawerProps = {
  type: 'displace',
  tapToClose: true,
  openDrawerOffset: 0.2, // 20% gap on the right side of drawer
  closedDrawerOffset: 0,
  //tweenHandler: (ratio) => ({
  //  main: { opacity: 1 },
  //  //mainOverlay: {
  //  //  opacity: ratio / 1.2,
  //  //  backgroundColor: 'black',
  //  //},
  //}),
};

const withDrawer = WrappedComponent => props => {
  const {
    navigationState: { children, isOpen },
    onNavigate
  } = props;

  return (
    <RNDrawer {...drawerProps}
              open={isOpen}
              content={<WrappedComponent />}
              onOpenStart={() => dismissKeyboard()}
              onCloseStart={() => dismissKeyboard()}>
      <DefaultRenderer onNavigate={onNavigate}
                       navigationState={children[ 0 ]} />
    </RNDrawer>
  );
};

export default withDrawer;