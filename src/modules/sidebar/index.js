import * as React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';

import styles from './styles';

import withDrawer from './drawer';
import Text from '../../common/text';

const menus = [
  { title: 'Home', scene: 'home' },
  { title: 'Stackoverflow', scene: 'stack' },
  { title: 'Log Out', scene: 'logout' },
];

class SideBar extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        {menus.map(this.renderMenu)}
      </View>
    );
  }
  
  renderMenu = (menu, key) => (
    <TouchableOpacity style={styles.menu}
                      activeOpacity={0.6}
                      key={key}
                      onPress={() => this.openScene(menu.scene)}>
      <Text bold style={styles.menuText}>{menu.title}</Text>
    </TouchableOpacity>
  );
  
  openScene = (scene) => {
    Actions[ scene ]({ type: ActionConst.REPLACE });
    this.context.drawer.toggle();
  };
}

SideBar.contextTypes = {
  drawer: React.PropTypes.object,
};

export default withDrawer(SideBar);