import * as React from 'react';
import { connect } from 'react-redux';
import { ListView, Dimensions, ActivityIndicator, View } from 'react-native';
import Text from '../../common/text';
import Spinner from '../../common/spinner';

import styles from './styles';
import * as Styles from '../../styles';

import * as stackSelectors from '../../redux/stack/selectors';
import * as stackActions from '../../redux/stack';

const renderHeader = (isFetching, error) => () => {
  if (error) {
    return <Text style={styles.errorText}>Error: {error}</Text>;
  }
  
  if (isFetching) {
    return (
      <View style={styles.loader}>
        <Spinner size="large" />
      </View>
    );
  }
  
  return null;
};

const renderFooter = (isFetching, error) => () => {
  if (error) {
    return null;
  }
  
  if (isFetching) {
    return (
      <View style={styles.loader}>
        <Spinner />
      </View>
    );
  }
  
  return null;
};

const mapStateToProps = state => ({
  questions: stackSelectors.getQuestions(state),
  isFetching: stackSelectors.isFetching(state),
  isInitialFetching: stackSelectors.isInitialFetching(state),
  error: stackSelectors.getError(state),
});

const mapDispatchToProps = dispatch => ({
  clear: () => dispatch(stackActions.clear()),
  initialFetch: () => dispatch(stackActions.initialFetchRequest()),
  fetch: () => dispatch(stackActions.fetchRequest()),
});

class Stack extends React.Component {
  
  state = {
    dataSource: new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1.id !== r2.id,
    }).cloneWithRows([], null),
  };
  
  componentWillMount() {
    this.props.clear();
  }
  
  componentDidMount() {
    this.props.initialFetch();
  }
  
  componentWillReceiveProps(nextProps) {
    if (this.props.questions !== nextProps.questions) {
      this.setState(state => ({
        ...state,
        dataSource: state.dataSource.cloneWithRows(nextProps.questions)
      }));
    }
  }
  
  renderRow = (data) => {
    return (
      <View style={styles.row} key={data.id}>
        <Text>{data.title}</Text>
      </View>
    )
  };
  
  renderSeparator = (sid, rid) => {
    return <View style={styles.separator} key={rid}/>
  };
  
  render() {
    const { dataSource } = this.state;
    const {
      error,
      isFetching,
      isInitialFetching,
    } = this.props;
    
    return (
      <ListView
        contentContainerStyle={styles.listView}
        scrollRenderAheadDistance={800}
        dataSource={dataSource}
        enableEmptySections={true}
        initialListSize={15}
        renderHeader={renderHeader(isInitialFetching, error)}
        renderFooter={renderFooter(isFetching, error)}
        renderRow={this.renderRow}
        renderSeparator={this.renderSeparator}
        onEndReached={() => this.props.fetch()}
      />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Stack);