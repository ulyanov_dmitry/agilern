import { StyleSheet } from 'react-native';
import * as Styles from '../../styles';

export default StyleSheet.create({
  row: {
    padding: 8,
    marginTop: 20,
  },
  separator: {
    height: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  errorText: {
    color: 'red',
    paddingTop: 12,
    paddingBottom: 12,
    alignSelf: 'center',
  },
  loader: {
    paddingTop: 20,
    paddingBottom: 20,
  },
});