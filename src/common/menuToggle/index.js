import * as React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Octicons';
import styles from './styles';

const MenuToggle = (props, context) => (
  <TouchableOpacity onPress={context.drawer.toggle}
                    style={styles.touchable}>
    <Icon name="grabber" style={styles.icon}/>
  </TouchableOpacity>
);

MenuToggle.contextTypes = {
  drawer: React.PropTypes.object,
};

export default MenuToggle;