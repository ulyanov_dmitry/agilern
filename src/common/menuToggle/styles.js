import { StyleSheet } from 'react-native';
import * as Styles from '../../styles';

export default StyleSheet.create({
  touchable: {
    paddingLeft: 12,
    alignItems: 'center',
  },
  icon: {
    fontSize: Styles.fontSizes.big + 3,
    alignSelf: 'center',
  }
});