import * as React from 'react';
import { ActivityIndicator } from 'react-native';

import * as Styles from '../../styles';

const Spinner = props => (
  <ActivityIndicator size={props.size || 'small'}
                     color={Styles.colors.primary} />
);

export default Spinner;