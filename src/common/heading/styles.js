import { StyleSheet } from 'react-native';
import * as Styles from '../../styles';

export default StyleSheet.create({
  default: {
    fontSize: Styles.fontSizes.bigger,
    marginBottom: 12,
  },
});