import * as React from 'react';
import Text from '../text';

import styles from './styles';

const Heading = props => {
  const {
    style: styleExtension,
    ...otherProps,
  } = props;
  
  return (
    <Text style={[ styles.default, styleExtension]}
          bold
          textAlign='center'
          {...otherProps} />
  );
};

export default Heading;