import { StyleSheet } from 'react-native';
import * as Styles from '../../styles';

export default StyleSheet.create({
  default: {
    borderRadius: 8,
    borderWidth: 2,
    borderColor: Styles.colors.primaryDarker,
    padding: 8,
    backgroundColor: Styles.colors.lightWithOpacity,
    height: Styles.fontSizes.normal + 16,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  text: {
    fontSize: Styles.fontSizes.normal,
    color: Styles.colors.darker,
  }
});