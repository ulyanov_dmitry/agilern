import * as React from 'react';
import {
  Dimensions,
  TouchableOpacity,
  View
} from 'react-native';
import Text from '../text';

import styles from './styles';
import { withOrientation } from '../../helper/orientation';

const Button = props => {
  const {
    text,
    onPress,
    orientation,
    ...otherProps
  } = props;
  
  const w = Dimensions.get('window');
  const width = w.width * (orientation === 'LANDSCAPE' ? 0.45 : 0.7);
  
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[ styles.default, { width } ]}>
        <Text style={styles.text}>{text}</Text>
      </View>
    </TouchableOpacity>
  )
};

Button.propTypes = {
  text: React.PropTypes.string.isRequired,
  onPress: React.PropTypes.func.isRequired,
};

export default withOrientation(Button);