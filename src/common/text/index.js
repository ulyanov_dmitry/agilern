import * as React from 'react';
import { Text as DefaultText } from 'react-native';

import styles from './styles';
import * as Styles from '../../styles';

const Text = (props) => {
  const {
    children,
    color,
    bold,
    italic,
    style: styleExtension,
    ...otherProps,
  } = props;
  const style = [
    styles.default,
    color && { color: Styles.getColor(color) },
    bold && styles.bold,
    italic && styles.italic,
    styleExtension,
  ];
  
  return (
    <DefaultText style={style}
                 allowFontScaling={false}
                 ellipsizeMode='tail'
                 {...otherProps}>
      {children}
    </DefaultText>
  );
};

Text.propTypes = {
  color: React.PropTypes.any,
  bold: React.PropTypes.bool,
  italic: React.PropTypes.bool,
};

export default Text;