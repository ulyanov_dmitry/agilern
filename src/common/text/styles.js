import { StyleSheet } from 'react-native';
import * as Styles from '../../styles';

export default StyleSheet.create({
  default: {
    fontSize: Styles.fontSizes.normal,
    fontFamily: Styles.fonts.lato,
    color: Styles.colors.dark,
  },
  bold: {
    fontWeight: '800',
  },
  italic: {
    fontStyle: 'italic',
  },
});