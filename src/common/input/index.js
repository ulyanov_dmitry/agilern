import * as React from 'react';
import { View, TextInput, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/Foundation';

import styles from './styles';
import * as Styles from '../../styles';
import { withOrientation } from '../../helper/orientation';

const availableIcons = [ 'torso', 'lock' ];

const renderIcon = iconName => <Icon name={iconName} style={styles.icon} />;

const Input = props => {
  const {
    icon,
    value,
    orientation,
    ...otherProps,
  } = props;
  
  const w = Dimensions.get('window');
  const width = w.width * (orientation === 'LANDSCAPE' ? 0.45 : 0.7);
  
  return (
    <View style={styles.container}>
      { icon && availableIcons.includes(icon) && renderIcon(icon) }
      <TextInput value={value}
                 underlineColorAndroid='transparent'
                 autoCapitalize='none'
                 style={[ styles.default, { width } ]}
                 placeholderTextColor={Styles.colors.primary}
                 {...otherProps} />
    </View>
  );
};

Input.propTypes = {
  value: React.PropTypes.string.isRequired,
  onChangeText: React.PropTypes.func.isRequired,
  icon: React.PropTypes.string,
  orientation: React.PropTypes.string,
  placeholder: React.PropTypes.string,
};

export default withOrientation(Input);