import { StyleSheet } from 'react-native';
import * as Styles from '../../styles';

export default StyleSheet.create({
  default: {
    fontSize: Styles.fontSizes.normal,
    borderRadius: 8,
    padding: 8,
    color: Styles.colors.darker,
    backgroundColor: Styles.colors.light,
    height: Styles.fontSizes.normal + 16,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
    marginBottom: 14,
    height: Styles.fontSizes.normal + 16,
  },
  icon: {
    alignSelf: 'center',
    alignItems: 'flex-start',
    marginRight: 6,
    color: Styles.colors.primary,
    fontSize: Styles.fontSizes.big + 2,
  }
});