import * as React from 'react';
import { View } from 'react-native';
import { DefaultRenderer } from 'react-native-router-flux';

const StatusBarPadding = props => {
  const {
    navigationState: { children },
    onNavigate
  } = props;
  
  return (
    <View style={{ flex: 1 }}>
      <View style={{ height: 20 }}/>
      <DefaultRenderer navigationState={children[ 0 ]}
                       onNavigate={onNavigate} />
    </View>
  );
};

export default StatusBarPadding;