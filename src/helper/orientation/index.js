import * as React from 'react';
import Orientation from 'react-native-orientation';

const createOrientationObservable = (initial) => {
  let orientation = initial;
  let subscribers = [];
  return {
    setOrientation(newOrientation) {
      orientation = newOrientation;
      subscribers.forEach(subscriber => subscriber(orientation));
    },
    subscribe(handler) {
      subscribers.push(handler);
    },
    unsubscribe(handler) {
      subscribers = subscribers.filter(x => x !== handler);
    },
    getOrientation() {
      return orientation;
    }
  };
};

export class OrientationProvider extends React.Component {
  static childContextTypes = {
    orientation: React.PropTypes.object,
  };
  
  getChildContext = () => {
    return {
      orientation: this.orientation
    };
  };
  
  orientationChanged = (newOrientation) => {
    console.log(`Orientation changed to ${newOrientation}`);
    this.orientation.setOrientation(newOrientation);
  };
  
  componentWillMount() {
    const initialOrientation = Orientation.getInitialOrientation();
    this.orientation = createOrientationObservable(initialOrientation);
  }
  
  componentDidMount() {
    Orientation.addOrientationListener(this.orientationChanged);
  }
  
  render() {
    return this.props.children;
  }
  
  componentWillUnmount() {
    Orientation.removeOrientationListener(this.orientationChanged);
  }
}

export const withOrientation = WrappedComponent => (
  class WithOrientation extends React.Component {
    static contextTypes = {
      orientation: React.PropTypes.object,
    };
    
    state = {
      orientation: null,
    };
    
    setOrientation = (value) => {
      this.setState(state => ({ ...state, orientation: value }));
    };
    
    componentWillMount() {
      const { orientation } = this.context;
      this.setOrientation(orientation.getOrientation());
      orientation.subscribe(this.setOrientation);
    }
    
    render() {
      const props = {
        ...this.props,
        orientation: this.state.orientation,
      };
      
      return <WrappedComponent {...props} />;
    }
    
    componentWillUnmount() {
      this.context.orientation.unsubscribe(this.setOrientation);
    }
    
  }
);