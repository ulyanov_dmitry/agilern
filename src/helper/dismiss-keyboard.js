import { TextInput } from 'react-native';

const { State } = TextInput;

export default dismissKeyboard = () => State.blurTextInput(State.currentlyFocusedField());