import { Platform } from 'react-native';

export const fonts = {
  gidole: 'Gidole',
  gidolinya: 'Gidolinya',
  lato: 'Lato',
};

export const fontSizes = {
  normal: 18,
  big: 24,
  bigger: 30,
};

export const colors = {
  light: '#F5F5F5',
  lighter: '#FAFAFA',
  lightGrey: '#CACACA',
  lightWithOpacity: 'rgba(240,240,240,0.85)',
  
  dark: '#424242',
  darker: '#212121',
  
  primaryLighter: '#80CBC4',
  primaryLighterWithOpacity: 'rgba(128, 203, 196, 0.6)',
  primary: '#009688',
  primaryDarker: '#00796B',
};

export const createShadow = () => {
  return Platform.select({
    ios: {
      shadowColor: '#000',
      shadowOpacity: 0.3,
      shadowRadius: 5,
      shadowOffset: { width: 0, height: 0 },
    },
    android: {
      elevation: 8,
    }
  })
};

export const getColor = colorName => colors[colorName] || colors.dark;