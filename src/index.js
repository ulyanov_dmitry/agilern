import * as React from 'react';
import { AppRegistry, Platform, StatusBar, } from 'react-native';

import Router from './router';
import { Provider } from 'react-redux';
import { configureStore } from './redux/store';
import { OrientationProvider } from './helper/orientation';

const store = configureStore();
Platform.OS === 'android' && StatusBar.setTranslucent(true);

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <OrientationProvider>
          <Router />
        </OrientationProvider>
      </Provider>
    );
  }
}

AppRegistry.registerComponent('agileTest', () => App);