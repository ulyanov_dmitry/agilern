import axios from 'axios';

const baseUrl = 'https://api.stackexchange.com/2.2/search';

export const createQuestionsFetcher = (searchPhrase, pageSize) => page => axios.get(baseUrl, {
  params: {
    page,
    pageSize,
    site: 'stackoverflow',
    intitle: searchPhrase,
  }
}).then(res => res.data);