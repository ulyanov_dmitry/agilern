import { AsyncStorage } from 'react-native';

export const getToken = () => AsyncStorage.getItem('token');

export const setToken = (value) => AsyncStorage.setItem('token', value);

export const removeToken = () => AsyncStorage.removeItem('token');

export const getProfile = () => AsyncStorage.getItem('profile').then(r => JSON.parse(r));

export const setProfile = (value) => AsyncStorage.setItem('profile', JSON.stringify(value));

export const removeProfile = () => AsyncStorage.removeItem('profile');

const users = [
  'Steve & rogers',
  'Elliot & robot',
];

export const authRequest = credentials => {
  const { username, password } = credentials;
  if (users.includes(`${username} & ${password}`)) {
    const response = {
      profile: {
        username
      },
      token: '12345',
    };
    return Promise.resolve({ response });
  } else {
    return Promise.resolve({ error: 'Credentials are wrong' });
  }
};

