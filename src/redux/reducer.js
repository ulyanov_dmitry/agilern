import { combineReducers } from 'redux';

import profile from './profile';
import { moduleName as profileModuleName } from './profile/config';
import stack from './stack';
import { moduleName as stackModuleName } from './stack/config';

export default combineReducers({
  [profileModuleName]: profile,
  [stackModuleName]: stack,
})