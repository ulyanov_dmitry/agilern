import { createAction, handleActions } from 'redux-actions';
import { moduleName } from './config';

const initialState = {
  token: null,
  fields: {},
  signInError: null,
};

export const SIGN_IN = `${moduleName}/SIGN_IN`;
export const signIn = createAction(SIGN_IN);

const SIGN_IN_FAILED = `${moduleName}/SIGN_IN_FAILED`;
export const signInFailed = createAction(SIGN_IN_FAILED);

const SIGN_IN_SUCCESS = `${moduleName}/SIGN_IN_SUCCESS`;
export const signInSuccess = createAction(SIGN_IN_SUCCESS);

export const SIGN_OUT = `${moduleName}/SIGN_OUT`;
export const signOut = createAction(SIGN_OUT);

const SIGN_OUT_SUCCESS = `${moduleName}/SIGN_OUT_SUCCESS`;
export const signOutSuccess = createAction(SIGN_OUT_SUCCESS);

export default handleActions({
  
  [SIGN_IN_FAILED]: (state, action) => ({
    ...state,
    signInError: action.payload,
  }),
  
  [SIGN_IN_SUCCESS]: (state, { payload }) => ({
    ...state,
    signInError: null,
    token: payload.token,
    fields: payload.profile,
  }),
  
  [SIGN_OUT_SUCCESS]: (state, action) => ({
    ...initialState,
  }),
  
}, initialState);