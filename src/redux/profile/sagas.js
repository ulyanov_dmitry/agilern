import { call, put, take } from 'redux-saga/effects';

import * as profileActions from './';
import * as auth from '../../services/auth';
import * as stackActions from '../stack';

function validateCredentials({ username, password }) {
  return (
    username && username.length > 0 &&
    password && password.length > 0
  );
};

function * authorization() {
  
  // Check the storage for cached token and profile
  let [ token, profile ] = yield [
    call(auth.getToken),
    call(auth.getProfile),
  ];
  
  while (true) {
    if (!token) {
      // Sign in request if no token in storage
      const { payload: credentials } = yield take(profileActions.SIGN_IN);
      
      const { response, error } = yield call(authRequest, credentials);
      if (error) {
        yield put(profileActions.signInFailed(error));
        continue;
      }
      
      token = response.token;
      profile = response.profile;
      
      // Saving token and profile in storage
      yield [
        call(auth.setToken, token),
        call(auth.setProfile, profile),
      ];
    }
    
    yield put(profileActions.signInSuccess({ token, profile }));
    
    yield take(profileActions.SIGN_OUT);
    
    token = profile = null;
    yield [
      call(auth.removeToken),
      call(auth.removeProfile),
    ];
    
    yield put(stackActions.clear());
    
    yield put(profileActions.signOutSuccess());
  }
}

function * authRequest(credentials) {
  if (!validateCredentials(credentials)) {
    return { error: 'Login and password should be filled!' };
  }
  
  return yield call(auth.authRequest, credentials);
}

export default [
  authorization,
];