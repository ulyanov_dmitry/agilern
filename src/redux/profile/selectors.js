import { moduleName } from './config';

export const getUsername = (state) => state[moduleName].fields.username;