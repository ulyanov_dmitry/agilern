import { fork } from 'redux-saga/effects';
import profile from './profile/sagas';
import stack from './stack/sagas';

const forker = saga => fork(saga);

export default function * rootSaga() {
  yield [
    ...profile,
    ...stack,
  ].map(forker);
};