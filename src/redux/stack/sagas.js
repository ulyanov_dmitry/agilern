import { call, put, take, takeLatest } from 'redux-saga/effects';

import * as stackActions from './';
import * as stack from '../../services/stack';

const searchPhrase = 'react native';

const pageSize = 30;

function * fetchRequestWatcher() {
  yield takeLatest(stackActions.INITIAL_FETCH_REQUEST, fetchSaga);
}

function * fetchSaga() {
  const fetchQuestions = stack.createQuestionsFetcher(searchPhrase, pageSize);
  let page = 1;
  let canLoadMore = true;
  let response = null;
  let questions = null;
  
  try {
    yield put(stackActions.initialFetchStart());
    
    response = yield call(fetchQuestions, page++);
    questions = response.items.map(({ question_id: id, title }) => ({ id, title }));
    canLoadMore = response.has_more;
    
    yield put(stackActions.fetchSuccess(questions));
    yield put(stackActions.initialFetchEnd());
  
    while (true) {
      yield take(stackActions.FETCH_REQUEST);
      yield put(stackActions.fetchingStarted());
      
      response = yield call(fetchQuestions, page);
      questions = response.items.map(({ question_id: id, title }) => ({ id, title }));
      canLoadMore = response.has_more;
      
      yield put(stackActions.fetchSuccess(questions));
      yield put(stackActions.fetchingEnded());
      
      if (!canLoadMore) {
        break;
      }
    }
  } catch (error) {
    console.log('fetch error %O', error);
    yield put(stackActions.fetchFailure(error));
  }
}

export default [
  fetchRequestWatcher,
];