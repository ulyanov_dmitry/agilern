import { createAction, handleActions } from 'redux-actions';
import { moduleName } from './config';

const initialState = {
  reactNativeQuestions: [],
  initialFetching: false,
  fetching: false,
  error: null,
};

const CLEAR = `${moduleName}/CLEAR`;
export const clear = createAction(CLEAR);

export const INITIAL_FETCH_REQUEST = `${moduleName}/INITIAL_FETCH_REQUEST`;
export const initialFetchRequest = createAction(INITIAL_FETCH_REQUEST);

const INITIAL_FETCH_START = `${moduleName}/INITIAL_FETCH_START`;
export const initialFetchStart = createAction(INITIAL_FETCH_START);

const INITIAL_FETCH_END = `${moduleName}/INITIAL_FETCH_END`;
export const initialFetchEnd = createAction(INITIAL_FETCH_END);

export const FETCH_REQUEST = `${moduleName}/FETCH_REQUEST`;
export const fetchRequest = createAction(FETCH_REQUEST);

const FETCH_SUCCESS = `${moduleName}/FETCH_SUCCESS`;
export const fetchSuccess = createAction(FETCH_SUCCESS);

const FETCH_FAILURE = `${moduleName}/FETCH_FAILURE`;
export const fetchFailure = createAction(FETCH_FAILURE);

const FETCHING_STARTED = `${moduleName}/FETCHING_STARTED`;
export const fetchingStarted = createAction(FETCHING_STARTED);

const FETCHING_ENDED = `${moduleName}/FETCHING_ENDED`;
export const fetchingEnded = createAction(FETCHING_ENDED);

export default handleActions({
  
  [CLEAR]: (state, action) => ({
    ...initialState,
  }),
  
  [FETCH_SUCCESS]: (state, action) => ({
    ...state,
    reactNativeQuestions: [ ...state.reactNativeQuestions, ...action.payload ],
  }),
  
  [INITIAL_FETCH_START]: (state, action) => ({
    ...state,
    initialFetching: true,
  }),
  
  [INITIAL_FETCH_END]: (state, action) => ({
    ...state,
    initialFetching: false,
  }),
  
  [FETCHING_STARTED]: (state, action) => ({
    ...state,
    fetching: true,
  }),
  
  [FETCHING_ENDED]: (state, action) => ({
    ...state,
    fetchingEnded: false,
  }),
  
}, initialState);