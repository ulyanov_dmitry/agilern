import { moduleName } from './config';

export const getQuestions = state => state[moduleName].reactNativeQuestions;

export const isFetching = state => state[moduleName].fetching;

export const isInitialFetching = state => state[moduleName].initialFetching;

export const getError = state => state[moduleName].error;
