import * as React from 'react';
import { connect } from 'react-redux';
import { Actions, Router, Scene, Switch, } from 'react-native-router-flux';
import StatusBarPaddingHOC from './helper/status-bar-padding';

// Unauthorized scenes
import LoginScreen from './modules/login';

// Authorized scenes
import SideBar from './modules/sidebar';
import Home from './modules/home';
import Stack from './modules/stack';
import Logout from './modules/logout';

// Common
import MenuToggle from './common/menuToggle';

const createScene = (key, props) => {
  return (
    <Scene key={key}
           sceneStyle={{ paddingTop: 64 }}
           renderBackButton={() => <MenuToggle />}
           renderLeftButton={() => <MenuToggle />}
           {...props} />
  );
};

const ConnectedRouter = connect()(Router);

const UnauthorizedScenes = (
  <Scene key="loginScenes">
    <Scene
      key="status-bar-padding"
      component={StatusBarPaddingHOC}
      hideNavBar>
      <Scene key="login" hideNavBar component={LoginScreen} />
    </Scene>
  </Scene>
);

const AuthorizedScenes = (
  <Scene key="mainScenes">
    <Scene key="drawer" component={SideBar}>
      <Scene key="wrapper">
        {createScene('home', { component: Home, title: 'Home', initial: true })}
        {createScene('stack', { component: Stack, title: 'Stackoverflow' })}
        {createScene('logout', { component: Logout, title: 'Log Out' })}
      </Scene>
    </Scene>
  </Scene>
);

const scenes = Actions.create(
  <Scene
    key="root"
    component={connect(state => ({ profile: state.profile }))(Switch)}
    tabs={true}
    unmountScenes
    selector={props => props.profile.token ? 'mainScenes' : 'loginScenes'}>
    {UnauthorizedScenes}
    {AuthorizedScenes}
  </Scene>
);

export default () => <ConnectedRouter scenes={scenes} />;